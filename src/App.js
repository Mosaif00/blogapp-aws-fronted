import { useContext } from "react";
import { Context } from "./globalContext/Context";
import { render } from "react-dom";

import {
  BrowserRouter as Router,
  Route,
  Switch,
  Link
} from "react-router-dom";

import TopBar from "./components/TopBar";
import Home from "./pages/home/Home";
import Login from "./pages/login/Login";
import NewBlog from "./pages/newBlog/NewBlog";
import Register from "./pages/register/Register";
import Setting from "./pages/settings/Setting";
import SinglePage from "./pages/singlePage/SinglePage";

function App() {
  const {user} = useContext(Context);

  return (
    <Router>
      <TopBar />
      <Switch>
        <Route path='/' exact>
          <Home/>
        </Route>
        <Route path='/login' >
          {user? <Home/>: <Login/>}
        </Route>
        <Route path='/register'>
         {user? <Home/>: <Register/>}
        </Route>
        <Route path='/write-new-blog'>
          {user? <NewBlog/>: <Register/>}
        </Route>
        <Route path='/settings'>
          {user? <Setting/>: <Register/>}
        </Route>
        <Route path='/post/:postId'>
          <SinglePage/>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
