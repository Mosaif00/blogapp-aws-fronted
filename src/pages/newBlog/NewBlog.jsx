import axios from 'axios';
import React, { useContext } from 'react';
import { useState } from 'react';
import ErrorMessage from '../../components/ErrorMessage';
import SuccessMessage from '../../components/SuccessMessage';
import { Context } from '../../globalContext/Context';
import './newblog.css';

const NewBlog = () => {
    const [title, setTitle] = useState('');
    const [desc, setDesc] = useState('');
    const [file, setFile] = useState('');
    const [errorCreateBlog, setErrorCreateBlog] = useState(false);
    const [successBlog, setSuccessBlog] = useState(false);

    const { user } = useContext(Context);

    const handlePostSubmit = async (e) => {
        e.preventDefault();
        setErrorCreateBlog(false);
        setSuccessBlog(false);

        const newBlog = {
            username: user.username,
            title,
            desc,
        };
        if (file) {
            const data = new FormData();
            const fileName = Date.now() + file.name;
            data.append('name', fileName);
            data.append('file', file);
            newBlog.img = fileName;
            try {
                await axios.post('/upload', data);
            } catch(error){console.log(error)}
        }
        try {
            const response = await axios.post('/posts', newBlog);
            setSuccessBlog(true);

            setInterval(() => {
                window.location.replace('/post/' + response.data._id);
            }, 1000);
        } catch (error) {
            setErrorCreateBlog(true);
        }

    };
    return (
        <div className='new-blog-container'>

            <form action="" className="blog-form" onSubmit={handlePostSubmit}>

                <div className="form-title">
                    <input className='title-input' type="text" placeholder='Post title' autoFocus={true} onChange={(e) => setTitle(e.target.value)} />

                </div>
                <div className="form-upload">
                    <label className='file-upload' htmlFor="fileUpload">Upload Post Image</label>
                    <input type="file" id='fileUpload' onChange={e => setFile(e.target.files[0])} />
                </div>

                <div className="form-description">
                    <textarea type='text' className='description-input' placeholder='Enter post content' onChange={(e) => setDesc(e.target.value)}></textarea>
                    {errorCreateBlog && <ErrorMessage message={"sorry, Could't create a post"} />}
                    {successBlog && <SuccessMessage message={"Post Created"} />}

                </div>

                <button className='submit-btn' type='submit'>Post Now</button>

            </form >
            {file &&
                <div className="form-image">
                    <img src={URL.createObjectURL(file)} alt="single post img" className="post-img" />
                </div>
            }
        </div >
    );
};

export default NewBlog;
