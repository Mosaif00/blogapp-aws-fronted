import axios from 'axios';
import React from 'react';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import ErrorMessage from '../../components/ErrorMessage';
import SuccessMessage from '../../components/SuccessMessage';
import './register.css';
const Register = () => {
    const [userName, setUserName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [successRegister,setSuccessRegister] = useState(false)
    const [errorMessage,setErrorMessage] = useState(false)

    const handleSubmit = async (e) => {
        setErrorMessage(false)
        setSuccessRegister(false)
        e.preventDefault();
        try {
            const response = await axios.post('/auth/register', {
                username: userName,
                email,
                password
            });
            if (response.status === 200) {
                setSuccessRegister(true)
                setErrorMessage(false)
            }
            setInterval(() => {
                return response.data && window.location.replace('/login')
            }, 1000);
        } catch (error) {
            setErrorMessage(true)
            setSuccessRegister(false)

        }

    };
    return (
        <div className='register-container'>
            <span className="register-title">Register</span>
       
            <form action="" className="register-form" onSubmit={handleSubmit}>
                <label >Username</label>
                <input type="text" onChange={e => setUserName(e.target.value)} placeholder='Enter your username' />
                <label >Email</label>
                <input type="text" onChange={e => setEmail(e.target.value)} placeholder='Enter your email' />
                <label >Password</label>
                <input type="password" onChange={e => setPassword(e.target.value)} placeholder='Enter your password' />
           
                <button className='register-btn' type='submit'>
                    <Link className='link-special-style' to='/register'>Register</Link>
                </button>
            </form>
            {
                    errorMessage && <ErrorMessage message={'Something went wrong, try again.'}/>
                }
                {
                    successRegister && <SuccessMessage message={'Registration went successfully.'}/>
                }
            <button className='login-btn'>
                <Link className='link-special-style' to='/login'>Login</Link>
            </button>
        </div>
    );
};

export default Register;
