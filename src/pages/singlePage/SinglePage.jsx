import axios from 'axios';
import React, { useState } from 'react'
import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import Sidebar from '../../components/Sidebar'
import SinglePost from '../../components/SinglePost'
import './singlepage.css'

const SinglePage = () => {
  
    return (
        <div className='single-page-container'>
            <SinglePost />
            <Sidebar/>
        </div>
    )
}

export default SinglePage
