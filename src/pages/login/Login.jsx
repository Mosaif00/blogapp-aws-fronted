import axios from 'axios';
import React, { useContext, useRef } from 'react';
import { Link } from 'react-router-dom';
import { LOGIN_ERROR, LOGIN_START, LOGIN_SUCCESS } from '../../globalContext/Constants';
import { Context } from '../../globalContext/Context';
import './login.css';

const Login = () => {

    const emailRef = useRef();
    const passwordRef = useRef();

    const { dispatch, isLoading } = useContext(Context);

    const handleSubmit = async (e) => {
        e.preventDefault();
        dispatch({ type: LOGIN_START });

        try {
            const response = await axios.post('/auth/login', {
                email: emailRef.current.value,
                password: passwordRef.current.value,
            });
            dispatch({ type: LOGIN_SUCCESS, payload: response.data });
        } catch (error) {
            dispatch({ type: LOGIN_ERROR });
        }
    };
    return (
        <div className='login-container'>
            <span className="login-title">Login</span>
            <form action="" className="login-form" onSubmit={handleSubmit}>
                <label >Email</label>
                <input type="text" ref={emailRef} placeholder='Enter your email' />
                <label >Password</label>
                <input type="password" ref={passwordRef} placeholder='Enter your password' />
                <button className='login-btn' type='submit' disabled={isLoading}>
                    <Link className='link-special-style' to='/login'>Login</Link>

                </button>
            </form>
            <button className='register-btn'>
                <Link className='link-special-style' to='/register'>Register</Link>

            </button>
        </div>
    );
};

export default Login;
