import React, { useState } from 'react';
import Sidebar from '../../components/Sidebar';
import './settings.css';
import DriveFolderUploadIcon from '@mui/icons-material/DriveFolderUpload';
import { useContext } from 'react';
import { Context } from '../../globalContext/Context';
import axios from 'axios';
import SuccessMessage from '../../components/SuccessMessage';
import { UPDATE_USER_ERROR, UPDATE_USER_START, UPDATE_USER_SUCCESS } from '../../globalContext/Constants';
const Setting = () => {
    const [file, setFile] = useState(null);
    const [userName, setUserName] = useState('');
    const [userEmail, setUserEmail] = useState('');
    const [userPassword, setUserPassword] = useState('');
    const [updateSuccess, setUpdateSuccess] = useState(false);

    const { user, dispatch } = useContext(Context);
 // environment variables
 const publicFolder = 'http://localhost:8000/images/';

    console.log(user);
    const handleUserUpdate = async (e) => {
        e.preventDefault();
        dispatch({ type: UPDATE_USER_START });

        const updatedUser = {
            userId: user._id,
            username: userName || user.username,
            email: userEmail || user.email,
            password: userPassword || user.password
        };
        if (file) {
            const data = new FormData();
            const fileName = Date.now() + file.name;
            data.append('name', fileName);
            data.append('file', file);
            updatedUser.profilePicture = fileName;
            try {
                await axios.post('/upload', data);
            } catch (error) { console.log(error); }
        }
        try {
            const response = await axios.put(`/users/` + user._id, updatedUser);

            // return window.location.reload();

            setUpdateSuccess(true);
            dispatch({ type: UPDATE_USER_SUCCESS , payload: response.data});

            setInterval(() => {
                setUpdateSuccess(false);

            }, 2000);
        } catch (error) {
            dispatch({ type: UPDATE_USER_ERROR });

        }


    };

    return (
        <div className='setting-container'>
            <div className="settings-wrapper">
                <div className="setting-title">
                    <span className="settings-update-title">
                        Update Account
                    </span>
                    <span className="settings-delete-title">
                        Delete Account
                    </span>
                </div>
                <form action="" className='setting-form' onSubmit={handleUserUpdate}>
                    <label >Profile Picture</label>
                    <div className="setting-profile-picture">
                        <img className='profile-img' src={file ? URL.createObjectURL(file) : publicFolder+user.profilePicture} alt='profile logo' />
                        {/* <img className='profile-img' src={file**user.profilePicture} alt='profile logo' /> */}
                        <label className='dp-upload' htmlFor="dpUpload"><DriveFolderUploadIcon className='dp-upload-icon' /></label>
                        <input type="file" id='dpUpload' onChange={e => setFile(e.target.files[0])} />
                    </div>
                    <label>Username</label>
                    <input type="text" placeholder={user.username} onChange={e => setUserName(e.target.value)} />
                    <label>Email</label>
                    <input type="text" placeholder={user.email} onChange={e => setUserEmail(e.target.value)} />
                    <label>Password</label>
                    <input type="password" onChange={e => setUserPassword(e.target.value)} />
                    {updateSuccess && <SuccessMessage message={'updated'} />}
                    <button className='submit-setting' type='submit'>UPDATE</button>
                </form>
            </div>
            <Sidebar />
        </div>
    );
};

export default Setting;
