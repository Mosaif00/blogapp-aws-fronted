import { useEffect, useState } from 'react';
import Header from '../../components/Header';
import Posts from '../../components/Posts';
import Sidebar from '../../components/Sidebar';
import axios from 'axios';
import './home.css';
import { useLocation } from 'react-router-dom';

const Home = () => {
    const [posts, setPosts] = useState([]);

    const location = useLocation();
    const userQuery = location.search;

    useEffect(() => {
        const getPosts = async () => {
            const response = await axios.get('/posts' + userQuery);
            return setPosts(response.data);
        };
        return getPosts();
    }, [userQuery]);

    return (
        <>
            <Header />
            <div className='main-home'>
                <Posts posts={posts} />
                <Sidebar />
            </div>
        </>

    );
};

export default Home;
