import React from 'react'
import './styles/errorSuccessMessage.css'
const SuccessMessage = ({message}) => {
    return (
        <div className='message-container'>
           <h5 className='success error-success-message' >{message}</h5> 
        </div>
    )
}

export default SuccessMessage
