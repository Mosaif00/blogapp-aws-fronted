import React from 'react'
import './styles/header.css'
import HeaderImg from './images/headerImg.jpeg'

const Header = () => {
    return (
        <div className='header-container'>
            <div className="header-title">
                {/* <span className='title'>Blog</span> */}
            </div>
            <img src={HeaderImg} className='header-img' alt='header img'/>
        </div>
    )
}

export default Header
