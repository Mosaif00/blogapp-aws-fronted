import React from 'react'
import './styles/errorSuccessMessage.css'

const ErrorMessage = ({message}) => {
    return (
        <div className='message-container'>
           <h5 className='error error-success-message'>{message}</h5> 
        </div>
    )
}

export default ErrorMessage
