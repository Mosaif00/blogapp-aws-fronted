import React from 'react';
import {Link} from 'react-router-dom'
import './styles/post.css';
import postImage1 from './images/post1.png';
import postImage2 from './images/post2.png';
import postImage3 from './images/post3.jpeg';
import postImage4 from './images/post4.png';
import postImage5 from './images/post5.png';
// const publicFolder = process.env.PUBLIC_FOLDER


const Post = ({ post }) => {
        // environment variables
    const publicFolder = 'http://localhost:8000/images/'
    
    return (
        <div className='post-container'>
            {post.img &&
                <img src={publicFolder+post.img} className="post-img" alt={post.title} />
            }
            <div className="info-container">
                <div className="category">
                    {post.categories.map(category => (
                        <span key={category} className="post-categories">{category}</span>

                    ))}
                    <span className="created-date">{new Date(post.createdAt).toDateString()}</span>
                </div>
                <Link to={`/post/${post._id}`} className='link-special-style'>

                <span className="post-title">
                    {post.title}
                </span>
                </Link>
            </div>
            <p className='post-desc'>{post.desc}</p>
        </div>
    );
};

export default Post;
