import React, { useContext } from 'react';
import './styles/topbar.css';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import LanguageIcon from '@mui/icons-material/Language';
import SearchIcon from '@mui/icons-material/Search';
import { Link } from 'react-router-dom';
import { Context } from '../globalContext/Context';
import { USER_LOGOUT } from '../globalContext/Constants';

const TopBar = () => {
    const { user, dispatch } = useContext(Context);

     // environment variables
 const publicFolder = 'http://localhost:8000/images/';

    const handleLogout = () => {
        dispatch({ type: USER_LOGOUT });
    };

    return (
        <div className='top-bar-container'>
            <div className="top-left-container"><LinkedInIcon className='social-icons' />
                <LanguageIcon className='social-icons' /></div>
            <div className="top-center-container">
                <ul className='nav-list'>
                    <li className='nav-items'>
                        <Link className='link-special-style' to='/'>HOME</Link>
                    </li>
                
                    <li className='nav-items'>
                        <Link className='link-special-style' to='/write-new-blog'>NEW BLOG</Link>
                    </li>

                </ul>
            </div>
            <div className="top-right-container">
                {
                    user ? (
                        <>
                            <span className='logout' onClick={handleLogout}>LOGOUT</span>
                            <Link to='/settings'>
                                <img className='profile-logo' src={publicFolder+user.profilePicture} alt='profile logo' />
                            </Link>
                        </>
                    ) : (
                        <Link className='link-special-style' to='/login'>LOGIN/REGISTER</Link>
                    )
                }
                <SearchIcon className='search-icon' /></div>
        </div>
    );
};

export default TopBar;
