import axios from 'axios';
import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import './styles/sidebar.css';
const Sidebar = () => {
    const [categories, setCategories] = useState([]);

    useEffect(() => {
        const getCategories = async () => {
            const response = await axios.get('/categories');
            setCategories(response.data);
        };
        getCategories();
    }, []);
    return (
        <div className='sidebar-container'>
            <div className="sidebar-items">
                <span className="sidebar-title">
                    About DevSecOps Academy
                </span>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ipsa optio odit deleniti vel officiis perspiciatis nisi officia accusamus enim atque. Ex libero itaque ratione ea sint corporis voluptates natus fugit?</p>
            </div>
            <div className="sidebar-items">
                <span className="sidebar-title">Categories</span>
                <ul className="categories-list">
                    {categories.map((category) => (
                    <Link key={category.name} to={`/?category=${category.name}`} className='link-special-style'>
                    <li  className="category-item">{category.name}</li>
                    </Link>
                    ))}
                </ul>
            </div>
        </div>
    );
};

export default Sidebar;
