import axios from 'axios';
import React, { useContext, useState } from 'react';
import { useEffect } from 'react';
import { Link, useLocation } from 'react-router-dom';
import './styles/singlepost.css';
import singlePostImg from './images/post1.png';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { Context } from '../globalContext/Context';

// const publicFolder = process.env.REACT_APP_BASE_URL
// const {REACT_APP_BASE_URL} = process.env
const SinglePost = () => {
    const [singlePost, setSinglePost] = useState({});
    const [editTitle, setEditTitle] = useState('');
    const [editDesc, setEditDesc] = useState('');
    const [toUpdate, setToUpdate] = useState(false);

    const location = useLocation();
    const postPathId = location.pathname.split('/')[2];

    const { user } = useContext(Context);

    // environment variables
    const publicFolder = 'http://localhost:8000/images/';

    useEffect(() => {
        const getSinglePost = async () => {
            const response = await axios.get(`/posts/${postPathId}`);
            setSinglePost(response.data);
            setEditTitle(response.data.title);
            setEditDesc(response.data.desc);
        };
        return getSinglePost();
    }, [postPathId]);

    const handleDelete = async () => {
        console.log(user.username);
        // setToUpdate('true')
        try {
            await axios.delete(`/posts/${singlePost._id}`, {
                data: {
                    username: user.username
                }
            });
            return window.location.replace('/');
        } catch (error) {
            console.log(error);
        }

    };

    const handleEditPost = async () => {
        try {
            await axios.put(`/posts/${singlePost._id}`, {
                username: user.username,
                title: editTitle,
                desc: editDesc

            });
             setToUpdate(false)
        } catch (error) {
            console.log(error);
        }
    };



    return (
        <div className='single-post-container'>
            <div className="single-pose-wrapper">
                {singlePost.img &&
                    <img src={publicFolder + singlePost.img} alt="single post img" className="single-post-img" />
                }
                {
                    toUpdate ?
                        (<input type='text' className='single-post-title' value={editTitle} onChange={(e) => setEditTitle(e.target.value)} />)
                        : (
                            <h1 className='single-post-title'>{editTitle}
                                {singlePost.username === user?.username && (

                                    <div className="edit-delete-post">
                                        <EditIcon className='edit-delete-post-icons' onClick={() => setToUpdate(true)} />
                                        <DeleteIcon className='edit-delete-post-icons' onClick={handleDelete} />
                                    </div>
                                )

                                }
                            </h1>
                        )
                }

                <div className="single-post-info">
                    <span className="author-name"> Author:
                        <Link to={`/?user=${singlePost.username}`} className='link-special-style'>
                            <b>{singlePost.username}</b>
                        </Link>
                    </span>
                    <span className="post-date"> {new Date(singlePost.createdAt).toDateString()} </span>
                </div>
                {
                    toUpdate ? (
                        <textarea className='post-description' value={editDesc} onChange={e => setEditDesc(e.target.value)} />
                    ) :
                        (
                            <p className='post-description'>{editDesc}</p>

                        )
                }
                {
                    toUpdate && <button className='submit-btn' onClick={handleEditPost}>Edit</button>
                }

            </div>

        </div>
    );
};

export default SinglePost;
