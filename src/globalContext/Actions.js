import { LOGIN_START, LOGIN_SUCCESS,LOGIN_ERROR,USER_LOGOUT, UPDATE_USER_START, UPDATE_USER_SUCCESS, UPDATE_USER_ERROR } from "./Constants";

export const  LoginStart = (userCred) =>({
    type: LOGIN_START
})
export const LoginSuccess = (user) =>({
    type: LOGIN_SUCCESS,
    payload: user,
})
export const LoginError = () =>({
    type: LOGIN_ERROR,
})
export const logout = () =>({
    type: USER_LOGOUT,
})

export const UpdatedUserStart = (userCred) =>({
    type: UPDATE_USER_START
})
export const  UpdateUserSuccess = (user) =>({
type: UPDATE_USER_SUCCESS,
    payload: user,
})
export const UpdateUserError = () =>({
    type: UPDATE_USER_ERROR,
})