import { LOGIN_ERROR, LOGIN_START, LOGIN_SUCCESS, UPDATE_USER_ERROR, UPDATE_USER_START, UPDATE_USER_SUCCESS, USER_LOGOUT } from "./Constants";

const RootReducer = (state, action) => {
    switch (action.type) {
        case LOGIN_START:
            return {
                user: null,
                isLoading: true,
                isError: false,
            };
        case LOGIN_SUCCESS:
            return {
                user: action.payload,
                isLoading: false,
                isError: false,
            };
        case LOGIN_ERROR:
            return {
                user: null,
                isLoading: false,
                isError: true,
            };
        case USER_LOGOUT:
            return {
                user: null,
                isLoading: false,
                isError: false,
            };
        case UPDATE_USER_START:
            return {
                ...state,
                isLoading: true,
            };
        case UPDATE_USER_SUCCESS:
            return {
                user: action.payload,
                isLoading: false,
                isError: false,
            };
        case UPDATE_USER_ERROR:
            return {
                ...state,
                isLoading: false,
                isError: true,
            };
        default:

            break;
    }
};

export default RootReducer;