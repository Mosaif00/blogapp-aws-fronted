import { createContext, useEffect, useReducer } from "react";
import RootReducer from "./Reducer";

const INITIAL_STATE = {
    user: JSON.parse(localStorage.getItem('user')) || null,
    isLoading: false,
    isError: false,
};

export const Context = createContext(INITIAL_STATE);

export const ContextProvider = ({ children }) => {
    const [state, dispatch] = useReducer(RootReducer, INITIAL_STATE);

    useEffect(() => {
        return localStorage.setItem('user', JSON.stringify(state.user));
    }, [state.user]);

    return (
        <Context.Provider
            value={
                {
                    user: state.user,
                    isLoading: state.isLoading,
                    isError: state.isError,
                    dispatch
                }
            }
        >
            {children}
        </Context.Provider>
    );
};